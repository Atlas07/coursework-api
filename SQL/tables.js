export const departments = `SELECT
    department_id,
    department_name 
    FROM s_departments`
export const positions = `SELECT 
    position_id,
    position_name,
    position_is_manager 
    FROM s_positions`
export const businessTrips = `SELECT
    business_trip_id,
    business_trip_began_date, 
    business_trip_end_date, 
    business_trip_reason,
    business_trip_destination,
    business_trip_additional_info
    FROM s_business_trips`
export const courses = `SELECT 
    course_id,
    course_type, 
    course_name,
    course_company_conducted,
    course_is_distant
    FROM s_courses`
