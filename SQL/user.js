export const info = `SELECT  
    e.name, 
    e.surname, 
    e.phone, 
    e.email, 
    d.department_name, 
    p.position_name
    FROM s_employees e
    INNER JOIN s_departments d ON e.employee_department_id = d.department_id
    INNER JOIN s_positions p ON e.employee_position_id = p.position_id`

export const courses = `SELECT
    c.course_type, 
    c.course_name, 
    c.course_company_conducted, 
    c.course_is_distant
    FROM s_employees e
    INNER JOIN s_courses c ON e.employee_course_id = c.course_id`

export const trips = `SELECT 
    b.business_trip_began_date, 
    b.business_trip_end_date, 
    b.business_trip_reason,
    b.business_trip_destination, 
    b.business_trip_additional_info
    FROM s_employees e
    INNER JOIN s_business_trips b ON e.employee_business_trip_list_id = b.business_trip_id`

export const sicknessList = `SELECT 
    s.sickness_list_start_date, 
    s.sickness_list_end_date, 
    s.sickness_list_additional_info
    FROM s_employees e
    INNER JOIN s_sickness_lists s ON e.employee_sickness_list_id = s.sickness_list_id`
