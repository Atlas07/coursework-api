import express from "express"
import bodyParser from "body-parser"
import cors from "cors"
import mysql from "mysql"

import * as userQueries from "./SQL/user"
import * as generalQueries from "./SQL/tables"
import * as insertQueries from "./SQL/insert"
import * as deleteQueries from "./SQL/delete"
import * as updateQueries from "./SQL/update"

const app = express()

app.use(bodyParser.json())
app.use(cors())

const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "12345678",
  database: "dbLessons"
})

db.connect(err => {
  if (err) throw err

  app.post("/login", (req, res) => {
    const { email, password } = req.body

    db.query(
      "SELECT * FROM `s_employees` WHERE `email` = ? AND `employee_id` = ?",
      [email, +password],
      (errQuery, data) => {
        if (errQuery) {
          res.status(500).json({ errors: errQuery })
        } else if (data.length !== 1) {
          res.status(500).json({ errors: "No such user" })
        } else {
          res.json(data[0])
        }
      }
    )
  })

  app.post("/table", (req, res) => {
    const { table } = req.body

    db.query(generalQueries[table], (errQuery, data) => {
      if (errQuery) {
        res.status(500).json({ errors: errQuery })
      } else {
        res.json(data)
      }
    })
  })

  app.post("/user", (req, res) => {
    const { id } = req.body

    db.query(
      `SELECT 
        e.name, 
        e.surname,
        p.position_is_manager
        FROM s_employees e
        INNER JOIN s_positions p ON e.employee_position_id = p.position_id
        WHERE employee_id = ${id}`,
      (errQuery, data) => {
        if (errQuery) {
          res.status(500).json({ errors: errQuery })
        } else {
          res.json(data[0])
        }
      }
    )
  })

  app.post("/user-info", (req, res) => {
    const { id, tab } = req.body
    const dbQuery = `${userQueries[tab]} WHERE employee_id = ${id}`

    db.query(dbQuery, (errQuery, data) => {
      if (errQuery) {
        res.status(500).json({ errors: errQuery })
      } else {
        res.json(data)
      }
    })
  })

  app.post("/add", (req, res) => {
    const { table, data } = req.body

    db.query(insertQueries[table], data, (errQuery, result) => {
      if (errQuery) {
        res.status(500).json({ errors: errQuery })
      } else {
        res.json(result)
      }
    })
  })

  app.post("/remove", (req, res) => {
    const { id, table } = req.body
    const deleteQuery = `${deleteQueries[table]} ${id}`

    db.query(deleteQuery, (errQuery, result) => {
      if (errQuery) {
        res.status(500).json({ errors: errQuery })
      } else {
        res.json(result)
      }
    })
  })

  app.post("/update", (req, res) => {
    const { table, data, id } = req.body

    db.query(`${updateQueries[table]} ${id}`, data, (errQuery, result) => {
      if (errQuery) {
        console.log(errQuery)
        res.status(500).json({ errors: errQuery })
      } else {
        res.json(result)
      }
    })
  })
})

const port = 3000

app.listen(port, err => {
  if (err) throw err

  console.log(`Running on localhost:${port}`)
})
