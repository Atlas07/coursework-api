# Coursework API

## Get started

1. Clone git repo:

```
git clone git_url
```

2. Install dependencies:

```
npm i
```

3. Start application

```
npm start

```
